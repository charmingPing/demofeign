package entity;

import java.util.Date;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 22:02 2020/7/5
 */
public class Photo {

    private String id;
    private String bh;
    private String state;
    private String filename;
    private byte[] filecontent;
    private String filetext;
    private Date updatetime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBh() {
        return bh;
    }

    public void setBh(String bh) {
        this.bh = bh;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getFilecontent() {
        return filecontent;
    }

    public void setFilecontent(byte[] filecontent) {
        this.filecontent = filecontent;
    }

    public String getFiletext() {
        return filetext;
    }

    public void setFiletext(String filetext) {
        this.filetext = filetext;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }
}
