package com.client;

import entity.Photo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 10:46 2020/7/5
 */
@FeignClient(name = "uploadServiceClient", url = "http://demofeignbusiness")
@RequestMapping("/uploadService")
public interface UploadServiceClient {

    @RequestMapping("/insertPhoto")
    int insertPhoto(@RequestBody Photo photo);

    @RequestMapping("/getPhoto")
    List<Map<String, Object>> getPhoto(@RequestBody Map<String, Object> param);
}
