package com.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Map;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 20:36 2020/6/29
 */
@FeignClient(name = "demoServiceClient", url = "http://demofeignbusiness")
@RequestMapping("/demoService")
public interface DemoServiceClient {

    @RequestMapping("/getDemo")
    Map<String, Object> getDemo(@RequestBody Map<String, Object> param);

    @RequestMapping("/getGridData")
    List<Map<String, Object>> getGridData(@RequestBody Map<String, Object> param);
}
