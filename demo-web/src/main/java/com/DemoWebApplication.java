package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 20:06 2020/6/29
 */
@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients("com.client")  // 扫描开启Feign
@EnableScheduling       // 开启定时任务
public class DemoWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWebApplication.class, args);
    }
}
