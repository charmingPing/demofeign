package com.message;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

/**
 * rabbitMq 消费者
 * Title: RabbitComsumer.java
 * @Description 
 * @author Administrator
 * @Date 2020年7月18日上午9:19:27
 * @Version 1.0
 */
@Component
public class RabbitmqComsumer {

	private static final Logger log = LoggerFactory.getLogger(RabbitmqComsumer.class);
	 
	/**
	 * 没有消费者，那么消息会一直在队列中
	 * 监听消费-但消费实例
	 * @param message
	 * @param channel
	 * @throws IOException
	 */
    @RabbitListener(queues = "${rabbitmq.user.queue}", containerFactory = "singleListenerContainer")
    public void comsumer1(Message message, Channel channel) throws IOException {
        // 采用手动应答模式, 手动确认应答更为安全稳定
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
        log.info("comsumer1: " + new String(message.getBody()));
    }
    
    @RabbitListener(queues = "${rabbitmq.order.queue}")
    public void comsumer2(Message message, Channel channel) throws IOException {
        // 采用手动应答模式, 手动确认应答更为安全稳定
        channel.basicAck(message.getMessageProperties().getDeliveryTag(), true);
        log.info("comsumer2: " + new String(message.getBody()));
    }
    
    /**
     * 单一生产者，多消费者
     * @param message
     */
    @RabbitListener(queues = "hello1")
    @RabbitHandler
    public void processReceiver1(String message) {
    	log.info("processReceiver1 监听 hello1  接收到消息  : " + message);
    }
    
    @RabbitListener(queues = "hello1")
    @RabbitHandler
    public void processReceiver2(String message) {
    	log.info("processReceiver2  监听 hello1  接收到消息  : " + message);
    }
    
    
    /**
     * 广播模式
     * @param message
     */
    @RabbitListener(queues = "fanout.A")
    @RabbitHandler
    public void processA(String message) {
    	log.info("fanout Receiver A  : " + message);
    }
    
    @RabbitListener(queues = "fanout.B")
    @RabbitHandler
    public void processB(String message) {
    	log.info("fanout Receiver B  : " + message);
    }
    
    @RabbitListener(queues = "fanout.C")
    @RabbitHandler
    public void processC(String message) {
    	log.info("fanout Receiver C  : " + message);
    }

    
}
