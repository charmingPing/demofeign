package com.message;

import java.time.LocalTime;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.AbstractJavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Description 
 * @author Administrator
 * @Date 2020年7月18日上午1:25:34
 * @Version 1.0
 */
@RestController
@RequestMapping("/rabbitmq")
public class RabbitmqProducer {
	
	@Autowired
    private Environment env;
	
	@Autowired
	private RabbitTemplate rabbitTemplate; 
	
    /**
     * 给hello队列发送消息
     */
	@RequestMapping("/sendUser")
    public void sendUser() {     
        String str = "发送一个消息。。。请接收";
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.setExchange(env.getProperty("rabbitmq.user.exchange"));
        rabbitTemplate.setRoutingKey(env.getProperty("rabbitmq.user.routing"));       
        Message message = MessageBuilder.withBody(str.getBytes())
        		.setDeliveryMode(MessageDeliveryMode.PERSISTENT).build();
        message.getMessageProperties()
        .setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, MessageProperties.CONTENT_TYPE_JSON); 
        rabbitTemplate.convertAndSend(message); 
    }
	
    /**
     * 抢单模型
     */
	@RequestMapping("/sendOrder")
    public void sendOrder() {     
        String str = "明天预计中到大雨，请注意防雨。";
        rabbitTemplate.convertAndSend(env.getProperty("rabbitmq.order.exchange"), str); 
    }
	
    /**
     * 广播消息，不指定routing Key，给交换机发消息
     * 所有绑定该交换机的队列都会收到消息
     */
	@RequestMapping("/sendFanout")
    public void sendFanout() {     
        String str = "明天预计中到大雨，请注意防雨。";
        rabbitTemplate.convertAndSend("fanoutExchange","", str);
    }
	
    /**
     * 单一生产者，多消费者
     */
	@RequestMapping("/sendHello1")
    public void sendHello1() {     
		for (int i = 0; i < 10; i++) {
			String str = "sendHello1: 发送消息 ，当前时间：" + LocalTime.now();
			rabbitTemplate.convertAndSend("hello1", str);			
		}
    }
	
    /**
     * 一对一
     */
	@RequestMapping("/sendHello2")
    public void sendHello2() {     
        String str = "sendHello2: 发送消息 ，当前时间：" + LocalTime.now();
        rabbitTemplate.convertAndSend("hello2", str);
    }

}
