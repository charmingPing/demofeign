package com.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.ChannelAwareMessageListener;
import org.springframework.stereotype.Component;

import com.rabbitmq.client.Channel;

/**
 * Title: OrderListener.java
 * @Description 
 * @author Administrator
 * @Date 2020年7月18日下午1:01:10
 * @Version 1.0
 */
@Component("orderListener")
public class OrderListener implements ChannelAwareMessageListener {
	
	private static final Logger log = LoggerFactory.getLogger(OrderListener.class);

	@Override
	public void onMessage(Message message, Channel channel) throws Exception {
		long tag = message.getMessageProperties().getDeliveryTag();
		try {
			byte[] body = message.getBody();
			String mobile = new String(body, "UTF-8");
			log.info("监听到抢单手机号： {}", mobile);
			channel.basicAck(tag, true); // 确认消费
		} catch (Exception e) {
			log.error("抢单异常： ", e.fillInStackTrace());
			channel.basicReject(tag, false);  // 确认消费
		}
	}

}
