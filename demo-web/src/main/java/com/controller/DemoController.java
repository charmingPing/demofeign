package com.controller;

import com.client.DemoServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 20:06 2020/6/29
 */
@RestController
@RequestMapping("/demo")
public class DemoController {

    @Autowired
    DemoServiceClient demoServiceClient;

    @RequestMapping("/test")
    public String test() {
        return "success";
    }

    @RequestMapping("/getDemo")
    public Map<String, Object> getDemo(@RequestParam Map<String, Object> param) {

        System.err.println("param: " + param);

        Map<String, Object> result = demoServiceClient.getDemo(param);
        System.err.println("result: " + result);

        return result;
    }

    @RequestMapping("/getGridData")
    public List<Map<String, Object>> getGridData(@RequestParam Map<String, Object> param) {

        System.err.println("param: " + param);

        List<Map<String, Object>> result = demoServiceClient.getGridData(param);
        System.err.println("result: " + result);

        return result;
    }
}
