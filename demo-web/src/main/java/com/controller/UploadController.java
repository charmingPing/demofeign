package com.controller;

import com.alibaba.fastjson.JSONObject;
import com.client.UploadServiceClient;
import entity.Photo;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.*;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 10:25 2020/7/5
 */
@RestController
@RequestMapping("/upload")
public class UploadController {

    @Autowired
    UploadServiceClient uploadServiceClient;

    @PostMapping("/upload")
    public int upload(MultipartFile file, String params, HttpServletRequest request) {
        String filePath = request.getSession().getServletContext().getRealPath("/");
        System.err.println("file: " + file);
        if (file == null) {
            return 0;
        }
        String filename = file.getOriginalFilename();
        String param = request.getParameter("params");
        System.err.println("filename: " + filename);
        System.err.println("params: " + params);
        System.err.println("param: " + param);
        Map<String, Object> par = JSONObject.parseObject(params);

        int res = 0;
        try {
            byte[] bytes = getByte(file);
            Photo photo = new Photo();
            photo.setId(UUID.randomUUID().toString());
            photo.setFilename(filename);
            photo.setState(par.get("state").toString());
            photo.setFilecontent(bytes);
            res = uploadServiceClient.insertPhoto(photo);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
        return res;
    }

    @RequestMapping("/getPhoto")
    public List<Map<String, Object>> getPhoto(HttpServletRequest request) {
        Map<String, Object> param = new HashMap<>();
        List<Map<String, Object>> res = uploadServiceClient.getPhoto(param);
        return res;
    }

    @RequestMapping("/exportWord")
    public void exportWord(HttpServletRequest request, HttpServletResponse response) {
        Configuration cfg = new Configuration();
        cfg.setEncoding(Locale.getDefault(), "utf-8");
        File file = new File("a.doc");
        try {
            cfg.setDirectoryForTemplateLoading(new File("src/main/resources/static"));
            Template template = cfg.getTemplate("abc.ftl");


            Map<String, Object> root = new HashMap<>();
            root.put("year", 2020);
            root.put("company", "运维检修公司");

            Writer out = new OutputStreamWriter(new FileOutputStream(file), "utf-8");
            template.process(root, out);
            out.close();
            String fileName = URLEncoder.encode("报告.doc", "UTF-8");
            response.setContentType("application/msword;charset=UTF-8");
            response.setHeader("Content-disposition", "attachment; filename=" + fileName);

            byte[] buffer = new byte[1024];
            InputStream fis = new FileInputStream(file); //文件输入流
            OutputStream os = response.getOutputStream();

            int len = 0;
            while((len = fis.read(buffer)) >0){
                os.write(buffer, 0, len);
            }
            fis.close();
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        } finally {
            if (file != null) {
                file.delete();
            }
        }

    }

    //MultipartFile转换为二进制
    public byte[] getByte(MultipartFile multipartFile) throws IOException {
        byte[] bytes = multipartFile.getBytes();
        return bytes;
    }
}
