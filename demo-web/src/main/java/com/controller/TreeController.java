package com.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.client.TreeServiceClient;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 10:25 2020/7/5
 */
@RestController
@RequestMapping("/jstree")
public class TreeController {
	
	@Autowired
	TreeServiceClient treeServiceClient;

    @RequestMapping("/getGridData")
    public List<Map<String, Object>> getGridData(@RequestParam Map<String, Object> param, HttpServletRequest request) {
        System.err.println("param: " + param);
        List<Map<String, Object>> res = treeServiceClient.getGridData(param);
        return res;
    }
    
    @RequestMapping("/getZTreeData")
    public List<Map<String, Object>> getZTreeData(@RequestParam Map<String, Object> param, HttpServletRequest request) {
    	List<Map<String, Object>> res = treeServiceClient.getTree(param);
        return res;
    }
}
