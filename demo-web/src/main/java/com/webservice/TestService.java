package com.webservice;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * Webservice 接口
 */
@WebService
public interface TestService {

    @WebMethod
    String test();

    @WebMethod
    String testParam(String a, String b);
}
