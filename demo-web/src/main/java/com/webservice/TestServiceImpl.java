package com.webservice;

import com.client.DemoServiceClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TestServiceImpl implements TestService {

    @Autowired
    DemoServiceClient demoServiceClient;

    @Override
    public String test() {
        Map<String, Object> map = new HashMap<>();
        Map<String, Object> res = demoServiceClient.getDemo(map);
        System.err.println("============ " + res);
        return "aaaaaaaaaaa";
    }

    @Override
    public String testParam(String a, String b) {
        return "参数为： " + a + "," + b;
    }
}
