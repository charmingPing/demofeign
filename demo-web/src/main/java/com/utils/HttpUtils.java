package com.utils;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 模拟Http 请求
 * @Author: linmeng
 * @Description:
 * @Date: Create in 22:55 2020/7/3
 */
@Component
public class HttpUtils {

    private static final Log LOG = LogFactory.getLog(HttpUtils.class);

    public static final String POST = "POST";
    public static final String GET = "GET";

    private static final String HTTP_URL = "http://192.168.0.101:8080/demo/";
    private static final Map<String, String> HEADER = new HashMap<>();

    static {
        HEADER.put("Accept", "*/*");
        HEADER.put("Connection", "keep-alive");
        HEADER.put("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.108 Safari/537.36");
        HEADER.put("Content-Type", "application/json;charset=UTF-8");
    }

    public static Map<String, Object> sendGet(String methodName, Map<String, Object> param) {
        return sendHttp(GET, methodName, param, Map.class);
    }

    public static Map<String, Object> sendGet(String methodName) {
        return sendHttp(GET, methodName, new HashMap<>(), Map.class);
    }

    public static Map<String, Object> sendPost(String methodName, Map<String, Object> param) {
        return sendHttp(POST, methodName, param, Map.class);
    }

    public static Map<String, Object> sendPost(String methodName) {
        return sendHttp(POST, methodName, new HashMap<>(), Map.class);
    }

    public static String sendRequest(String requestType, String methodName, Map<String, Object> param) {
        return sendHttp(requestType, methodName, param, String.class);
    }

    /**
     * GET 请求
     * @param methodName
     * @param param
     * @return
     */
    private static <T> T sendHttp(String requestType, String methodName, Map<String, Object> param, Class<T> resultType) {
        String result = "";       // 返回的结果
        BufferedReader in = null; // 读取响应输入流
        PrintWriter out;
        String requestUrl;
        HttpURLConnection connection = null;
        try {
            requestUrl = HTTP_URL + methodName;
            if (GET.equals(requestType)) {
                requestUrl += "?" + encodeBody(param);
            }
            LOG.info("=======  开始请求  =======");
            LOG.info("url: " + requestUrl);
            // 创建URL对象
            URL connURL = new URL(requestUrl);
            // 打开URL连接
            connection = (HttpURLConnection) connURL.openConnection();
            // 设置通用属性
            setHeader(connection, requestType);
            // 建立实际的连接
            connection.connect();

            if (POST.equals(requestType)) {
                out = new PrintWriter(connection.getOutputStream());
                // 将参数传入
                String jsonParams = param == null ? "{}" : JSONObject.toJSONString(param);
                out.write(jsonParams);
                out.flush();
            }
            // 获取结果
            result = getResult(connection, in);

            LOG.info("请求结果: " + result);
            LOG.info("=======  请求结束  =======");
        } catch (Exception e) {
            LOG.error("请求异常", e);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                LOG.error("BufferedReader响应输入流关闭异常 ", ex);
            }
            connection.disconnect();
        }
        if (resultType.equals(String.class))
            return (T) result;

        try {
            if ("".equals(result)) {
                if (resultType.equals(Map.class))
                    return (T) new HashMap<>();
                if (resultType.equals(List.class))
                    return (T) new ArrayList<>();
                return resultType.newInstance();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            return null;
        }
        return JSONObject.parseObject(result, resultType);
    }

    /**
     * 将map转换成请求格式
     * @param map
     * @return
     */
    private static String encodeBody(Map<String, Object> map) {
        return map.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue()).collect(Collectors.joining("&"));
    }

    private static void setHeader(HttpURLConnection connection, String requestType) throws ProtocolException {
        HEADER.forEach((k, v) -> connection.setRequestProperty(k, v));
        // 设置请求方式
        connection.setRequestMethod(requestType);
        // 设置是否向httpUrlConnection输出
        connection.setDoOutput(true);
        connection.setDoInput(true);
    }

    private static String getResult(HttpURLConnection connection, BufferedReader in) throws IOException{
        String result = "";
        // 定义BufferedReader输入流来读取URL的响应,并设置编码方式
        in = new BufferedReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        String line;
        // 读取返回的内容
        while ((line = in.readLine()) != null) {
            result += line;
        }
        return result;
    }


}
