package com.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.message.RabbitmqProducer;

/**
 * Title: UtilTest.java
 * @Description 
 * @author Administrator
 * @Date 2020年7月18日上午10:11:54
 * @Version 1.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilTest {

	@Autowired
	HttpUtils httpClient;

	@Autowired
	WebserviceUtils webserviceClient;
	
	@Autowired
	RabbitmqUtils rabbitmqUtils;
	
	@Autowired
	RabbitmqProducer rabbitmqProducer;
	
	@Test
	public void test1() {
		rabbitmqProducer.sendFanout();
	}

	
	public void test2() throws Exception {
//      String res = HttpClient.sendRequest(HttpClient.GET, "test", new HashMap<>());
//      System.err.println("res: " + res);
//      Map<String, Object> p = new HashMap<>();
//      p.put("id", "11111112323");
//      Map<String, Object> res = HttpClient.sendGet("getDemo", p);
//      System.err.println("res: " + res);
//      Object res = WebserviceClient.sendHttp("test");
//      System.err.println("res: " + res);
		StringBuffer sb = new StringBuffer();
		sb.append("<arg0>参数A</arg0>");
		sb.append("<arg1>参数B</arg1>");
		String res = webserviceClient.sendRequest("testParam", sb.toString());
		System.err.println("res: " + res);
	}
	
	
	public void test3() throws Exception {
		// 多线程
        ExecutorService executor = Executors.newCachedThreadPool();
        List<Callable<Object>> tasks = new ArrayList<>(); 
        for (int i = 0; i < 100; i++) {
        	tasks.add(() -> task());
        }
        List<Future<Object>> result = null;
        try {
            // 开始执行，是阻塞的，直到全部任务结束
            result = executor.invokeAll(tasks);
            // 关闭线程
            executor.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public String task() {
		String threadName = Thread.currentThread().getName();
		String uuid = UUID.randomUUID().toString().replace("-", "");
		rabbitmqUtils.sendMessageJson((threadName + " : " + uuid).getBytes());
		System.err.println(threadName + " 发送消息：" + uuid);
		return uuid;
	}
	


}
