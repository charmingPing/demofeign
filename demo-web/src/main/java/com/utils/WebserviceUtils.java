package com.utils;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;
import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 13:14 2020/7/4
 */
@Component
public class WebserviceUtils {

    public static final String WSDL = "http://localhost:8080/services/testService";

    public static final String TARGETNAMESPACE = "http://webservice.com/";

    private static final JaxWsDynamicClientFactory JDCF = JaxWsDynamicClientFactory.newInstance();

    public static Object sendHttp(String method, Object... param) {
        Client client = JDCF.createClient(WSDL + "?wsdl");
        QName qName = new QName(TARGETNAMESPACE, method);
        try {
            Object[] result = client.invoke(qName, param);
            System.out.println("result: " + result[0]);
            return result[0];
        } catch (Exception e) {
            return null;
        }
    }

    public static String sendRequest(String method, String soapParam) throws Exception {
        // WebService服务的地址
        URL url = new URL(WSDL);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        //是否具有输入参数
        conn.setDoInput(true);
        //是否输出输入参数
        conn.setDoOutput(true);
        //设置请求头（注意一定是xml格式）
        conn.setRequestProperty("content-type", "text/xml;charset=utf-8");
        // 构造请求体，符合SOAP规范（最重要的）
        String requestBody = getSoapRequest(method, soapParam);
        //获得一个输出流
        OutputStream out = conn.getOutputStream();
        out.write(requestBody.getBytes());
        //获得服务端响应状态码
        int code = conn.getResponseCode();
        StringBuffer sb = new StringBuffer();
        if(code == 200){
            //获得一个输入流，读取服务端响应的数据
            InputStream is = conn.getInputStream();
            byte[] b = new byte[1024];
            int len = 0;

            while((len = is.read(b)) != -1){
                String s = new String(b,0,len,"utf-8");
                sb.append(s);
            }
            is.close();
        }
        out.close();
        System.err.println("服务端响应数据为："+sb.toString());
        return sb.toString();
    }

    private static String getSoapRequest(String method, String soapParam) {
        StringBuffer sb = new StringBuffer();
        sb.append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:web=\"" + TARGETNAMESPACE + "\">");
        sb.append("   <soapenv:Header/>");
        sb.append("   <soapenv:Body>");
        if (soapParam == null || "".equals(soapParam)) {
            sb.append("<web:" + method + "/>");
        } else {
            sb.append("<web:" + method + ">");
            sb.append(soapParam);
            sb.append("</web:" + method + ">");
        }
        sb.append("   </soapenv:Body>");
        sb.append("</soapenv:Envelope>");


        return sb.toString();
    }

    public String getWeather(String city) {
        return null;
    }

}
