/**
 * 
 */
package com.utils;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.core.MessageDeliveryMode;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.AbstractJavaTypeMapper;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Title: RabbitmqUtils.java
 * @Description 
 * @author Administrator
 * @Date 2020年7月18日上午10:14:46
 * @Version 1.0
 */
@Component
public class RabbitmqUtils {

	@Autowired
    private Environment env;
	
	@Autowired
	private RabbitTemplate rabbitTemplate; 
	
	/**
	 * 发送消息
	 * @param bytes
	 */
	public void sendMessageJson(byte[] bytes) {
    	rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        rabbitTemplate.setExchange(env.getProperty("rabbitmq.order.exchange"));
        rabbitTemplate.setRoutingKey(env.getProperty("rabbitmq.order.routing"));       
        Message message = MessageBuilder.withBody(bytes)
        		.setDeliveryMode(MessageDeliveryMode.PERSISTENT).build();
        message.getMessageProperties()
        .setHeader(AbstractJavaTypeMapper.DEFAULT_CONTENT_CLASSID_FIELD_NAME, MessageProperties.CONTENT_TYPE_JSON); 
        rabbitTemplate.convertAndSend(message); 
	}
}
