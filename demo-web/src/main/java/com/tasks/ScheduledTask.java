package com.tasks;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务类
 */
@Component
public class ScheduledTask {

    private static final Log LOG = LogFactory.getLog(ScheduledTask.class);

    private int fixedDelayCount = 1;

    /**
     * cron = "0 0 8 * * *" : 表示每天8点执行
     * 不受系统时间影响
     */
    @Scheduled(cron = "0 0 10 * * *")
    public void test () {
        LOG.info("==fixedDelay: 第{" + fixedDelayCount++ + "}次执行");
    }

}
