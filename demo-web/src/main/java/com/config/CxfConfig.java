package com.config;

import com.webservice.TestService;
import org.apache.cxf.Bus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.xml.ws.Endpoint;

/**
 * cxf 发布webservice
 */
@Configuration
public class CxfConfig {

    @Autowired
    private Bus bus;

    @Autowired
    TestService testService;

    /**
     * 低版本cxf发布webservice会导致rest请求无法访问，要重新new ServletRegistrationBean()
     * 高版本不用
     */
    /*@Bean
    public ServletRegistrationBean dispatcherServlet() {
        return new ServletRegistrationBean(new CXFServlet(), "/services/*");
    }*/

    /** JAX-WS **/
    @Bean
    public Endpoint endpoint() {
        EndpointImpl endpoint = new EndpointImpl(bus, testService);
        endpoint.publish("/testService");
        return endpoint;
    }
}
