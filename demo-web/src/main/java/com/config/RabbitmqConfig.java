package com.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AcknowledgeMode;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.message.OrderListener;


/**
 * rabbitMq 配置类
 * @Description rabbitMQ 消息队列
 * @author Administrator
 * @Date 2020年7月18日上午1:46:05
 * @Version 1.0
 */
@Configuration
public class RabbitmqConfig {

	private static final Logger LOG = LoggerFactory.getLogger(RabbitmqConfig.class);
	
    @Autowired
    private Environment env;
 
    @Autowired
    private CachingConnectionFactory connectionFactory;
 
    @Autowired
    private SimpleRabbitListenerContainerFactoryConfigurer factoryConfigurer;
    
    /**
     * Auto - 自动、Manual - 手动、None - 无需确认
     * 默认情况下是“单消费实例”的配置，即“一个 listener 对应一个消费者”
     * 这种配置对于并发量不高的场景下是适用的,但是在对于秒杀系统、商城抢单等场景下可能会显得很吃力！
     * 管理  RabbitMQ监听器listener 的容器工厂
     * 单一消费者
     * @return
     */
    @Bean(name = "singleListenerContainer")
    public SimpleRabbitListenerContainerFactory listenerContainer(){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        factory.setConcurrentConsumers(1);
        factory.setMaxConcurrentConsumers(1);
        factory.setPrefetchCount(1);
        factory.setTxSize(1);
        factory.setAcknowledgeMode(AcknowledgeMode.AUTO);
        return factory;
    }
    
    /**
     * 管理  RabbitMQ监听器listener 的容器工厂
     * 多个消费者
     * @return
     */
    @Bean(name = "multiListenerContainer")
    public SimpleRabbitListenerContainerFactory multiListenerContainer(){
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factoryConfigurer.configure(factory,connectionFactory);
        factory.setMessageConverter(new Jackson2JsonMessageConverter());
        factory.setAcknowledgeMode(AcknowledgeMode.NONE);
        factory.setConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.concurrency",int.class));
        factory.setMaxConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.max-concurrency",int.class));
        factory.setPrefetchCount(env.getProperty("spring.rabbitmq.listener.simple.prefetch",int.class));
        return factory;
    }
    
    /**
     * 充当消息的发送组件
     * @return
     */
    @Bean
    public RabbitTemplate rabbitTemplate(){
        connectionFactory.setPublisherConfirms(true);
        connectionFactory.setPublisherReturns(true);
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMandatory(true);        
        // ConfirmCallback接口用于实现消息发送到RabbitMQ交换器后接收ack回调   即消息发送到exchange  ack
        rabbitTemplate.setConfirmCallback(new RabbitTemplate.ConfirmCallback() {
            @Override
            public void confirm(CorrelationData correlationData, boolean ack, String cause) {
            	LOG.info("消息发送成功:correlationData({}),ack({}),cause({})",correlationData,ack,cause);
            }
        });
        // ReturnCallback接口用于实现消息发送到RabbitMQ 交换器，但无相应队列与交换器绑定时的回调  即消息发送不到任何一个队列中  ack
        rabbitTemplate.setReturnCallback(new RabbitTemplate.ReturnCallback() {
            @Override
            public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
            	LOG.info("消息丢失:exchange({}),route({}),replyCode({}),replyText({}),message:{}",exchange,routingKey,replyCode,replyText,message);
            }
        });
        return rabbitTemplate;
    }
    
    /**
     * 一对一队列，没有交换机
     * @return
     */
    @Bean
    public Queue hello1Queue(){
    	return new Queue("hello1");
    }
    
    @Bean
    public Queue hello2Queue(){
    	return new Queue("hello2");
    }
    
    /**
     * Queue 可以有4个参数
     *      1.队列名
     *      2.durable       持久化消息队列 ,rabbitmq重启的时候不需要创建新的队列 默认true
     *      3.auto-delete   表示消息队列没有在使用时将被自动删除 默认是false
     *      4.exclusive     表示该消息队列是否只在当前connection生效,默认是false
     * 消息模型：DirectExchange+RoutingKey 消息模型
     * Direct Exchange是RabbitMQ默认的交换机模式，也是最简单的模式，根据key全文匹配去寻找队列
     * 
     * @return
     */
    @Bean(name = "userQueue")
    public Queue userQueue(){
    	return new Queue(env.getProperty("rabbitmq.user.queue"), true);
    }
    
    @Bean
    public DirectExchange userExchange(){
    	return new DirectExchange(env.getProperty("rabbitmq.user.exchange"), true, false);
    }
    
    @Bean
    public Binding userBinding(){
    	return BindingBuilder.bind(userQueue()).to(userExchange()).with(env.getProperty("rabbitmq.user.routing"));
    }
      
    /**
     * 自定义监听器
     * 抢单模型
     */
    @Autowired
    private OrderListener orderListener;
    
    /**
     * 并发量配置与消息确认机制
     * 消息模型：TopicExchange + RoutingKey 消息模型
     * @return
     */
    @Bean(name = "orderQueue")
    public Queue orderQueue(){
    	return new Queue(env.getProperty("rabbitmq.order.queue"), true);
    }
    
    @Bean
    public TopicExchange orderExchange(){
    	return new TopicExchange(env.getProperty("rabbitmq.order.exchange"), true, false);
    }
    
    @Bean
    public Binding logUserBinding(){
    	return BindingBuilder.bind(orderQueue()).to(orderExchange()).with(env.getProperty("rabbitmq.order.routing"));
    }
    
	@Bean
	@SuppressWarnings("deprecation")
    public SimpleMessageListenerContainer orderListenerContainer(@Qualifier("orderQueue") Queue orderQueue) {
    	SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
    	container.setConnectionFactory(connectionFactory);
    	container.setMessageConverter(new Jackson2JsonMessageConverter());
    	
    	// TODO 并发配置
    	container.setConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.concurrency",int.class));
    	container.setMaxConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.max-concurrency",int.class));
    	container.setPrefetchCount(env.getProperty("spring.rabbitmq.listener.simple.prefetch",int.class));
    	
    	// TODO 消息确认机制
    	container.setQueues(orderQueue);
    	container.setMessageListener(orderListener);
    	container.setAcknowledgeMode(AcknowledgeMode.MANUAL);
    	return container;
    }
	
	/**
	 * 
	 * 广播模式
	 */
    @Bean
    public Queue AMessage() {
        return new Queue("fanout.A");
    }
 
    @Bean
    public Queue BMessage() {
        return new Queue("fanout.B");
    }
 
    @Bean
    public Queue CMessage() {
        return new Queue("fanout.C");
    }
 
    @Bean
    public FanoutExchange fanoutExchange() {
        return new FanoutExchange("fanoutExchange");
    }
 
    @Bean
    public Binding bindingExchangeA(Queue AMessage,FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(AMessage).to(fanoutExchange);
    }
 
    @Bean
    public Binding bindingExchangeB(Queue BMessage, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(BMessage).to(fanoutExchange);
    }
 
    @Bean
    public Binding bindingExchangeC(Queue CMessage, FanoutExchange fanoutExchange) {
        return BindingBuilder.bind(CMessage).to(fanoutExchange);
    }

}
