$(function(){

	_init();

	
})

function _init() {
	
	_initTree();

	_initGrid();
}

// 初始化树
function _initTree() {
	var setting = {
		check: {
			// enable: true   // 显示、不显示 复选框
		},  
		data: {
			simpleData: {
				enable: true,
				idKey: "ID",   // 自己定义，大小写随意
				pIdKey: "PID",
				rootPId: 0,    // 根节点的父节点
			},
			key: {
				name: "NAME"  // 指定key值
			}
	    },
	    view: {
	    	showLine: true,  // 显示连接线
	    	showIcon: true,   // 显示节点图片
	    	fontCss: {color: "#18b2c1"}
	    },
	    async: {   // 异步请求数据 ,节点是父节点，且children为空时，点展开时 才会触发
	    	enable: true,
	    	url: "/jstree/getZTreeData",     //j
	    	type: "get",
	    	dataType: "json",     // 返回值类型，默认是文本"text"
	    	autoParam: ["ID","PID","TYPE","level"],   // treeNode 里面的key
	    	otherParam: {sss:123},          // 其他参数 ，["sss","123"]或者{sss:123} == 会转成格式sss=123
	    	dataFilter: filterEvent
	    },
	    callback: {
	    	beforeClick: zTreeBeforeClick,  // 树节点的点击事件
	    	onClick: onClickEvent,
	    	onAsyncSuccess: zTreeOnAsyncSuccess,
	    	onCheck: onCheckClick
	    }
	};
	// 后台请求数据，用于初始化树
	$.ajax({
		url: "/jstree/getZTreeData",
		data: {},
		type: "get",
		dataType: "json",
		success: function(data) {
			if (data && data.length) {
				// 初始化树
				$.fn.zTree.init($("#regionZTree"), setting, data); 
			}			
		}
	});
	
}

// 点击节点文字触发事件
function onClickEvent(event, treeId, treeNode, clickFlag) {
	type = treeNode.TYPE;
	id = treeNode.ID;
	var param = {
		id: treeNode.ID,
		type: treeNode.TYPE
	};
	refreshGrid(param);
	
}

// 点击节点文字触发
function zTreeBeforeClick(treeId, treeNode, clickFlag) {
	// 点击节点复选框会被自动勾上
	var treeObj = $.fn.zTree.getZTreeObj(treeId);
	treeObj.checkNode(treeNode, !treeNode.checked, true, true);
}

// 数据过滤事件
function filterEvent(treeId, parentNode, responseData) {
	
}

// 三角箭头点击展开节点-异步请求成功时触发
function zTreeOnAsyncSuccess(event, treeId, treeNode, responseData) {
	// 返回的数据是字符串转成对应object
	var data = eval(responseData);
	if (data && data.length) {
		var treeObj = $.fn.zTree.getZTreeObj(treeId);
		// 获取指定父节点
		var parentNode = treeObj.getNodeByParam("ID", treeNode.ID, null);
		var newNode = treeObj.addNodes(parentNode, data, false);
	}
}

// 复选框点击事件
function onCheckClick(e, treeId, treeNode) {
	// 设置复选框只能选择一个
	var treeObj = $.fn.zTree.getZTreeObj(treeId); 
	var status = treeNode.checked;
	treeObj.checkAllNodes(false);
	treeObj.checkNode(treeNode, status, false);
}

// 加载表格
function _initGrid() {
    // bootstrap table表格对象
    var option = {
        striped: true,  // 开启间隔行颜色
        undefinedText: '',//当数据为 undefined 时显示的字符
        sidePagination: true,
        pagination: true,//启动分页
        paginationLoop: true,  // 分页循环
        pageNumber: 1,
        pageSize: 5,
        showFooter: false,  // 页脚显示隐藏
        checkboxHeader: false,  // 表头全选框隐藏
        columns: [{
            checkbox: true,
            width: 80
        }, {
            field: 'number',
            title: '序号',
            align: 'center',
            formatter: function(value,row,index){
                return index + 1;
            }
        }, {
            field: 'EMPNO',
            title: '编号',
            align: 'center'
        }, {
            field: 'ENAME',
            title: '名称',
            align: 'center'
        }, {
            field: 'SAL',
            title: '工资',
            align: 'center'
        }, {
            field: 'JOB',
            title: '工作',
            align: 'center'
        }, {
            field: 'DNAME',
            title: '部门名称',
            align: 'center'
        }, {
            field: 'HIREDATE',
            title: '日期',
            align: 'center',
            formatter:function(value,row,index){
                var d = new Date(value).toJSON();
                var s = new Date(+new Date(d) + 8 * 3600 * 1000)
                    .toISOString().replace(/T/g, " ").replace(/\.[\d]{3}Z/,"");
                return s;
            }
        }, {
            title: '操作',
            field: 'caozuo',
            align: 'center',
            events: clickEditEvent,
            formatter:function(value,row,index){
                var e = '<a  style="display: block;" class="btn btn-warning btn-xs edit" οnclick="edit(\''+ index + '\')">编辑</a>';
                return e;
            }
        }],
        onLoadSuccess: showDetail,  // 加载成功执行
        onPageChange: showDetail    // 页数改变时触发
    }
    $("#table").bootstrapTable(option);
}

window.clickEditEvent = {
    "click .edit": function (e, value, row, index) {
        layer.open({
            type: 2,
            title: '详情页面',
            offset: 'rb', // 右下角弹出， t:上，b:下，l:左，r:右
            shade: false,
            // maxmin: true, //开启最大化最小化按钮
            area: ['50%', '50%'],
            content: '',              // //fly.layui.com/
            id: 'LAY_layuipro'        // 设置id, 一种类型只允许弹出一次，防止重复弹出
        });
    }
}

function showDetail() {
    $(".bootstrap-table tr td").each(function () {
        $(this).attr("title", $(this).text());
        $(this).css("cursor", "pointer");
    });
}

function refreshGrid(param) {
	$("#table").bootstrapTable('refresh', {
		url: '/jstree/getGridData', 
		query: param    // key不能用param, 会导致传到后台的参数只有order:asc,没有其他参数
	});
}
