$(function(){

    _init();

    loadGridData();

})

function _init() {
    $("#ceshi").click(function(){
        $.ajax({
            url: "/demo/getDemo",
            data: {id: "0000001"},
            type: "get",
            dataType: "json",
            success: function(data){
                debugger
            }
        });
    });

    $("#chaxun").click(function(){
        $("#table").bootstrapTable('refresh', {url: '/demo/getGridData', param: {}});
    });
    
    window.clickEditEvent = {
        "click .edit": function (e, value, row, index) {
            layer.open({
                type: 2,
                title: '详情页面',
                offset: 'rb', // 右下角弹出， t:上，b:下，l:左，r:右
                shade: false,
                // maxmin: true, //开启最大化最小化按钮
                area: ['50%', '50%'],
                content: '',              // //fly.layui.com/
                id: 'LAY_layuipro'        // 设置id, 一种类型只允许弹出一次，防止重复弹出
            });
        }
    }
}

function loadGridData() {
    // bootstrap table表格对象
    var option = {
        striped: true,  // 开启间隔行颜色
        undefinedText: '',//当数据为 undefined 时显示的字符
        sidePagination: true,
        pagination: true,//启动分页
        paginationLoop: true,  // 分页循环
        pageNumber: 1,
        pageSize: 5,
        showFooter: false,  // 页脚显示隐藏
        checkboxHeader: false,  // 表头全选框隐藏
        columns: [{
            checkbox: true,
            width: 80
        }, {
            field: 'number',
            title: '序号',
            align: 'center',
            formatter: function(value,row,index){
                return index + 1;
            }
        }, {
            field: 'EMPNO',
            title: '编号',
            align: 'center'
        }, {
            field: 'ENAME',
            title: '名称',
            align: 'center'
        }, {
            field: 'SAL',
            title: '工资',
            align: 'center'
        }, {
            field: 'JOB',
            title: '工作',
            align: 'center'
        }, {
            field: 'HIREDATE',
            title: '日期',
            align: 'center',
            formatter:function(value,row,index){
                var d = new Date(value).toJSON();
                var s = new Date(+new Date(d) + 8 * 3600 * 1000)
                    .toISOString().replace(/T/g, " ").replace(/\.[\d]{3}Z/,"");
                return s;
            }
        }, {
            title: '操作',
            field: 'caozuo',
            align: 'center',
            events: clickEditEvent,
            formatter:function(value,row,index){
                var e = '<a  style="display: block;" class="btn btn-warning btn-xs edit" οnclick="edit(\''+ index + '\')">编辑</a>';
                return e;
            }
        }],
        onLoadSuccess: showDetail,  // 加载成功执行
        onPageChange: showDetail    // 页数改变时触发
    }
    $("#table").bootstrapTable(option);
}

function showDetail() {
    $(".bootstrap-table tr td").each(function () {
        $(this).attr("title", $(this).text());
        $(this).css("cursor", "pointer");
    });
}

