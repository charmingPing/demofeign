// jquery 初始化
var menuList = [];
$(document).ready(function(){

	$(".first-menu>li").click(function(e){
		// 如果是缩放的，不可点击
		var is = $(this).hasClass("contractiveli");
		if (is) {
			return false;
		}
		// 二级菜单阻止冒泡
		var isSecond = $(e.target).parent("ul").hasClass("second-menu");
		if (isSecond) {
			return false;
		}
		
		$(this).siblings().each(function(){
			var obj = $(this).children(".menu");
			if (obj.hasClass("down-arrow")) {
				obj.removeClass("down-arrow").addClass("right-arrow");
			}
			$(this).children("ul").slideUp(500);
		});
		var obj = $(this).children(".menu");
		if (obj.hasClass("right-arrow")) {
			obj.removeClass("right-arrow").addClass("down-arrow");
		} else if (obj.hasClass("down-arrow")) {
			obj.removeClass("down-arrow").addClass("right-arrow");
		}
		$(this).children("ul").slideToggle(200);
	});
	
	/**左侧菜单栏的展开与缩小**/
	$(".left-expand").click(function(){
			
		var left = $("#left_nav");
		var right = $("#right_nav");
		var all = $(document.body).width();
		if (left.width() == 200) {
			hideMenu();			
			left.animate({width: "50px"}, {duration: 300, queue: false}, function(){});
			right.animate({"margin-left": "50px"}, {duration: 300, queue: false}, function(){});
			
			$(".first-menu>li").addClass("contractiveli");
		} else if (left.width() == 50) {	
			showMenu();
			left.animate({width: "200px"}, {duration: 300, queue: false}, function(){});
			right.animate({"margin-left": "200px"}, {duration: 300, queue: false}, function(){});	
			
			$(".first-menu>li").removeClass("contractiveli");
		}
		
	});
	
	$(".first-menu>li").mouseover(function(e){
		var s = event.fromElement || event.relatedTarget;
        if (!this.contains(s)) { 
        	// 方法体
        	
        } 
		
		var is = $(this).hasClass("contractiveli");	
		// $(this).attr('data-beforeContent', html); 动态修改after的问女本
		if (is) {

			// 阻止事件的冒泡
			if ($(e.target).hasClass("every-menu")) {
				return;
			}			
			// 全部删除，否则自己的这一层就生成很多
			$(".first-menu>li>.absolute-menu2").remove();
			var span = $(this).children(".menu").children("span");
			var nodes = $(this).children("ul").children("li");
			var html = '<div class="absolute-menu2"><span class="triangle-left"></span>';
			html += '<div class="every-menu">' + span.text() + '</div>';
			nodes.each(function(){
				html += '<div class="every-menu">' + $(this)[0].textContent + '</div>';
			});
			html += '</div>';
			$(this).append(html);
		}
	});
	
	
/*	在IE中，所有的HTML元素都有一个contains方法，它的作用是判断当前元素内部是否包含指定的元素。
	我们利用这个方法来判断外层元素的事件是不是因为内部元素而被触发，如果内部元素导致了不需要的事件被触发，那我们就忽略这个事件。
	event.fromElement指向触发onmouseover和onmouseout事件时鼠标离开的元素；
	event.toElement指向触发onmouseover和onmouseout事件时鼠标进入的元素。
	那么上面两行代码的含义就分别是：
	○ 当触发onmouseover事件时，判断鼠标离开的元素是否是当前元素的内部元素，如果是，忽略此事件；
	○ 当触发onmouseout事件时，判断鼠标进入的元素是否是当前元素的内部元素，如果是，忽略此事件；
	这样，内部元素就不会干扰外层元素的onmouseover和onmouseout事件了。*/

	// 移出触发  -- 悬浮触发,动态元素无法获取，给父级元素添加绑定事件
	$(".first-menu").on("mouseout", ".absolute-menu2", function(e){
		var s = event.toElement || event.relatedTarget;
        if (!this.contains(s)) { 
        	// 方法体
        	$(".first-menu>li>.absolute-menu2").remove();
        }
	});

	// 鼠标点击
	$(document).mousedown(function(e){
		// 悬浮菜单一直显示，当鼠标在其他地方点击时，将悬浮菜单去除
		if (!$(e.target).hasClass("every-menu")) {
			$(".first-menu>li>.absolute-menu2").remove();
		}
		
		if (!$(e.target).hasClass("nav-menu")) {
			$(".hori-ul>li").children(".fa-nav-menu").hide();
		}
	});
	
	// 悬浮时显示
	$(".hori-ul>li").mouseover(function(e){
		$(this).siblings().children(".fa-nav-menu").hide();
		$(this).children(".fa-nav-menu").show();
	});
	
	// 移出時隱藏
	$(".hori-ul>li").mouseout(function(e){
		$(this).children(".fa-nav-menu").hide();
	});
	
	// 移出触发  -- 悬浮触发,动态元素无法获取，给父级元素添加绑定事件
	$(".hori-ul>li").click(function(e){
        $(this).children(".fa-nav-menu").toggle();        	
	});
	
	// 右下角菜单
	$("#jiahao").click(function(e){
		if ($(this).hasClass("layui-icon-addition")) {
			$(this).removeClass("layui-icon-addition").addClass("layui-icon-subtraction");
			var others = $(this).prevAll();   // 倒序
			for (var i = 0; i < others.length; i++) {
				var oth = $(others[i]);
				oth.show();
				var top = oth.position().top - (oth.height() + 8) * (i + 1);
				console.log(i + ":" + top + "," + top * (i + 1));
				oth.animate({"top": top + "px"}, 200, null, function(){
					console.log("*********");
				});	
			}
		} else {
			$(this).removeClass("layui-icon-subtraction").addClass("layui-icon-addition");
			var others = $(this).prevAll();   // 倒序
			for (var i = 0; i < others.length; i++) {
				var oth = $(others[i]);
				var top = oth.position().top + (oth.height() + 8) * (i + 1);
				oth.animate({"top": top + "px"}, 200, null, function(){
					// 只有第一个有效，其他回调能进入，css没有触发效果
					others.hide();
					console.log(i +"回调");
				});	
			}
		}
	});
	
	// 二级菜单点击事件
	$(".second-menu>li").click(function(){
		var url = $(this).attr("url");
		showLoading();
		$("#iframe_main").attr("src", url);
		hideLoading();
	});
	
});



/*隐藏菜单*/
function hideMenu() {
	// 先关闭之前打开的菜单
	$(".first-menu>li").each(function(){
		var obj = $(this).children(".menu");
		if (obj.hasClass("down-arrow")) {
			obj.removeClass("down-arrow").addClass("right-arrow");
		}
		$(this).children("ul").slideUp(300);
	});
	
	var objs = $(".first-menu>li>.menu").each(function(){		
		if ($(this).hasClass("right-arrow")) {
			$(this).removeClass("right-arrow");
			$(this).addClass("is-arr");
		}
		$(this).removeClass("left-ani");
		$(this).next("ul").addClass("absolute-ul");
		$(this).find("span").addClass("absolute-span").hide();
	});
}

/*展示菜单*/
function showMenu() {
	var objs = $(".first-menu>li>.menu").each(function(){
		if ($(this).hasClass("is-arr")) {
			$(this).removeClass("is-arr");
			$(this).addClass("right-arrow");
		}
		$(this).addClass("left-ani");
		$(this).next("ul").removeClass("absolute-ul");
		$(this).find("span").removeClass("absolute-span").css("display", "inline-block");
	});
}


//展示loading框
function showLoading() {
	$("#load").show();
}
//隐藏掉loading框
function hideLoading() {
	$("#load").hide();
}




