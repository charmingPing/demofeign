$(function(){

    _init();
});

function _init(){
    _initEvent();
}

function _initEvent(){
    $('#sc').on('click', upload);

    $('#file').change(function(){
        var file = $(this).val();
        if ($(this).val() == '') {
            return;
        }
        var formData = new FormData($('#formData')[0]);
        var params = {
            id: new Date().getFullYear(),
            state: 0
        }
        formData.append('params', JSON.stringify(params));
        $.ajax({
            url: '/upload/upload',
            data: formData,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function(data){
                debugger
            }
        });
    });

    $('#cx').click(function(){
        $.ajax({
            url: '/upload/getPhoto',
            data: '',
            type: 'get',
            dataType: 'json',
            success: function(data){
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        $("#ccc").append('<img src="data:image/jpg;base64,' + data[i].FILECONTENT + '" style="width:50%;height:300px;"/>');
                    }
                }
            }
        });
    });
    
    $('#fasong').click(function(){
    	 $.ajax({
             url: '/rabbitmq/sendMessage',
             data: '',
             type: 'get',
             dataType: 'json',
             success: function(data){

             }
         });
    });
}

function upload(){
    console.log('strat select file');
    $('#file').val('');
    $('#file').trigger('click');
}