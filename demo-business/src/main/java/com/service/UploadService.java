package com.service;

import entity.Photo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 10:54 2020/7/5
 */
@RestController
@RequestMapping("/uploadService")
public class UploadService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * @RequestBody Map<String, Object> param byte[] 会出现问题？
     * 使用feign请求 byte[] 不能放在map中
     * 参数使用实体类的形式BLOB类型字段在实体类中对应byte[]
     * @param photo
     * @return
     */
    @RequestMapping("/insertPhoto")
    public int insertPhoto(@RequestBody Photo photo) {
        String sql = "insert into photo (id, state, filename, filecontent, updatetime) values (?,?,?,?, sysdate)";
        List<Object> args = new ArrayList<>();
        args.add(photo.getId());
        args.add(photo.getState());
        args.add(photo.getFilename());
        args.add(photo.getFilecontent());
        int res = jdbcTemplate.update(sql, args.toArray());
        return res;
    }

    @RequestMapping("/getPhoto")
    public List<Map<String, Object>> getPhoto(@RequestBody Map<String, Object> param) {
        String sql = "select * from photo";
        List<Map<String, Object>> res = jdbcTemplate.queryForList(sql);
        return res;
    }
}
