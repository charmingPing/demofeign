package com.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 20:42 2020/6/29
 */
@RestController
@RequestMapping("/demoService")
public class DemoService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/getDemo")
    public Map<String, Object> getDemo(@RequestBody Map<String, Object> param) {

        String sql = "select * from emp where empno = ? ";
        Map<String, Object> list = jdbcTemplate.queryForMap(sql.toString(), "7400");
        return list;
    }

    @RequestMapping("/getGridData")
    public List<Map<String, Object>> getGridData(@RequestBody Map<String, Object> param) {
        String sql = "select * from emp ";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString());
        return list;
    }
}
