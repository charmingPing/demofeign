package com.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: linmeng
 * @Description: 树形结构及表格演示
 * @Date: Create in 20:42 2020/6/29
 */
@RestController
@RequestMapping("/treeService")
public class TreeService {
	
	private static final Log LOG = LogFactory.getLog(TreeService.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/getTree")
    public List<Map<String, Object>> getTree(@RequestBody Map<String, Object> param) {
    	System.err.println("param: " + param);
    	
    	String level = (String) param.get("level");
    	String type = (String) param.get("TYPE");
    	List<Map<String, Object>> result = new ArrayList<>();
    	if (level == null) {
    		Map<String, Object> map = new HashMap<>();
    		map.put("ID", "all");
    		map.put("NAME", "部门单位");
    		map.put("isParent", true);
    		map.put("TYPE", "BM");
    		result.add(map);
    		return result;
    	}
    	StringBuffer sql = new StringBuffer();
    	List<Object> paramList = new ArrayList<>();
    	if ("0".equals(level)) {
    		sql.append(" select t.deptno id, t.dname name, 'DEPT' type  ");
    		sql.append(" from dept t ");
    		sql.append(" group by t.deptno, t.dname ");    		
    		sql.append(" order by t.deptno ");    		
    	} else if ("1".equals(level)) {
    		sql.append(" select t.empno id, t.ename name, 'MGR' type  ");
    		sql.append(" from emp t ");
    		sql.append(" where t.deptno = ? ");
    		paramList.add(param.get("ID"));
    	}
    	try {
    		result = jdbcTemplate.queryForList(sql.toString(), paramList.toArray());	
		} catch (Exception e) {
			LOG.error("树查询异常", e);
		}
    	if (result != null && !result.isEmpty()) {
    		for (Map<String, Object> map : result) {
    			map.put("isParent", true);	
    			if ("DEPT".equals(type)) {
    				map.put("isParent", false);	
    			}
    		}
    	}
    	return result;
    }

    @RequestMapping("/getGridData")
    public List<Map<String, Object>> getGridData(@RequestBody Map<String, Object> param) {
    	String type = (String) param.get("type");
    	
    	StringBuffer sql = new StringBuffer();
    	List<Object> paramList = new ArrayList<>();
    	if (type == null || "BM".equals(type)) {
    		sql.append("select * from emp ");    		
    	} else if ("DEPT".equals(type)) {
    		sql.append("select e.*, d.dname from emp e join dept d on e.deptno = d.deptno ");  
    		sql.append("where e.deptno = ? ");
    		paramList.add(param.get("id"));
    	} else if ("MGR".equals(type)) {
    		sql.append("select e.*, d.dname from emp e join dept d on e.deptno = d.deptno ");  
    		sql.append("where e.empno = ? ");
    		paramList.add(param.get("id"));
    	} 
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql.toString(), paramList.toArray());
        return list;
    }

}
