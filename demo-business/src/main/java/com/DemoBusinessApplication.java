package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @Author: linmeng
 * @Description:
 * @Date: Create in 20:27 2020/6/29
 */
@SpringBootApplication
@EnableEurekaClient
public class DemoBusinessApplication {
    public static void main(String[] args) {

        SpringApplication.run(DemoBusinessApplication.class, args);
    }
}
